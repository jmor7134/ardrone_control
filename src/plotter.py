#!/usr/bin/python

import rospy
from geometry_msgs.msg import Point
import math
import numpy as np
import matplotlib.pyplot as plt
from threading import Lock

plot_lock = Lock()

path_pts = []
estimated_pts_x = []
estimated_pts_y = []

estimated_path, = plt.plot([], [], '-r', label='estimated path')
closest_point, = plt.plot([], [], marker='o', color='b', label='closest point')

def fx(x):
  x *= 2
  return 0.85*math.sin(2*x) + 0.52*math.sin(x)

def path_pt(point):
  plot_lock.acquire()
  closest_point.set_xdata(point.x)
  closest_point.set_ydata(point.y)
  plot_lock.release()

def estimated_pt(point):
  plot_lock.acquire()
  estimated_pts_x.append(point.x)
  estimated_pts_y.append(point.y)
  estimated_path.set_ydata(estimated_pts_y)
  estimated_path.set_xdata(estimated_pts_x)
  plot_lock.release()

def main():
  rospy.init_node('ardrone_control_plotter')
  rospy.Subscriber('/ardrone_control/closest_path_pt', Point, path_pt)
  rospy.Subscriber('/ardrone_control/estimated_pt', Point, estimated_pt)

  # Plot fx.
  X = np.linspace(0, np.pi, 256, endpoint=True)
  F = np.vectorize(fx)(X)

  plt.ion()
  plt.plot(X, F, '-k')
  plt.axis([-0.2, np.pi + 0.2, -1.5, 1.5])
  plt.draw()
  plt.show()
  print 'abc'

  rate = rospy.Rate(10)
  while not rospy.is_shutdown():
    plot_lock.acquire()
    plt.draw()
    plt.show()
    plot_lock.release()
    rate.sleep()

if __name__ == '__main__':
  main()
