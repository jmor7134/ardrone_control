#include "LQT.h"

using Matrix = Eigen::MatrixXd;
using Vector = Eigen::VectorXd;

LQT::LQT(
  Matrix A, Matrix B, Matrix C, Matrix Q, Matrix R, Matrix P, Matrix K,
  std::chrono::nanoseconds frequency,
  C::MEASUREMENT_FUNCTION measurement,
  C::SETPOINT_FUNCTION setpoint,
  C::UPDATE_FUNCTION update)
  : Controller(frequency, measurement, setpoint, update)
  , _K(K) {
  // Calculate the constants.
  _C1 = A.transpose() - P*B*R.inverse()*B;
  _C2 = C.transpose()*Q;
  _C3 = R.inverse()*B.transpose();
}

LQT::~LQT() {

}

Vector LQT::DoControl(const Vector& state, const Vector& setpoint) {
  ROS_INFO_STREAM("u = " << -(_K * state));
  ROS_INFO_STREAM("Ku = " << -Ku(state, setpoint));

  return -(_K * state) - Ku(state, setpoint);
}

const Matrix& LQT::Ku(const Vector& state, const Vector& setpoint) {
  UpdateKu(state, setpoint);
  return _Ku;
}

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/algebra/vector_space_algebra.hpp>

using namespace boost::numeric::odeint;
using stepper =
  runge_kutta_dopri5<Vector, double, Vector, double, vector_space_algebra>;

void LQT::UpdateKu(const Vector& state, const Vector& setpoint) {
  // Calculate the ~y vector.
  Vector y = setpoint;

  auto lqt = [this](const Vector& p, Vector& dpdt, const double t) {
    Vector ytilde = _setpoint(t);
    dpdt = -(_C1*p - _C2*ytilde);
  };

  Vector p(6);
  integrate_const(stepper(), lqt, p, 20.0, fmod(TimeSinceStart(), 20.0), -0.01);

  // Update Ku.
  _Ku = -_C3*p;
}
