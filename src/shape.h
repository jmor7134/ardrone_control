#pragma once

#include <Eigen/Dense>

#include <json/json.h>

#include <ros/ros.h>

#include <cmath>
#include <fstream>
#include <sstream>

class Shape {
 public:
  virtual ~Shape() {}

  // Get the point on this shape that is proportion% through it.
  // For example, GetPoint(50) of a line will return the center of the line.
  //              GetPoint(100) of a circle will return the end (start) of it.
  // It is required that 0 <= proportion <= 100
  virtual Eigen::Vector3d GetPoint(double proportion) const = 0;

  // Load a shape from the JSON representation. This method will not take
  // ownership of the created pointer.
  static Shape* Load(const Json::Value& spec);
};

// Circles are rooted at the bottom, so GetPoint(0) == GetPoint(100) == bottom
// point
class Circle : public Shape {
 public:
  enum Direction {
    CLOCK_WISE=1, ANTI_CLOCK_WISE=-1
  };

  enum Anchor {
    TOP=270, LEFT=180, RIGHT=0, BOTTOM=90
  };

  Circle(Eigen::Vector3d center, double radius,
         Direction direction=CLOCK_WISE, Anchor anchor=BOTTOM);
  virtual ~Circle();

  // The proportion in this case correspondings to the angle from 0:2pi.
  // This angle is relative to the BOTTOM of the circle.
  virtual Eigen::Vector3d GetPoint(double proportion) const;

  static Circle* Load(const Json::Value& spec);

 private:
  Eigen::Vector3d _center;
  double _radius;
  Direction _direction;
  Anchor _anchor;
};

// A fixed point in space.
class Point : public Shape {
 public:
  Point(Eigen::Vector3d point);
  virtual ~Point();

  virtual Eigen::Vector3d GetPoint(double proportion) const;

  static Point* Load(const Json::Value& spec);

 private:
  Eigen::Vector3d _point;
};
