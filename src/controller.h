#pragma once

#include <ros/ros.h>

#include <chrono>
#include <functional>
#include <memory>
#include <ratio>
#include <thread>

///
/// Interface to a controller, to allow it to be stored easily in an array.
///
class ControllerInterface {
 public:
  virtual void Enable() = 0;
  virtual void Disable() = 0;
};

///
/// A generic controller. Used by the common main the run the controller of
/// choice.
///
template <typename STATE_TYPE, typename OUTPUT_TYPE>
class Controller : public ControllerInterface {
 public:
  typedef std::function<STATE_TYPE()> MEASUREMENT_FUNCTION;
  typedef std::function<STATE_TYPE(double)> SETPOINT_FUNCTION;
  typedef std::function<void(const OUTPUT_TYPE&)> UPDATE_FUNCTION;

  Controller() {};
  Controller(
    std::chrono::nanoseconds frequency,
    MEASUREMENT_FUNCTION measurement,
    SETPOINT_FUNCTION setpoint,
    UPDATE_FUNCTION update);
  virtual ~Controller();

  ///
  /// Enable the controller. This will start any threads associated with the
  /// controller itself and begin controlling.
  ///
  void Enable();

  ///
  /// Disable the controller. This should stop all threads (not immediately) so
  /// that the program can exit. This call should block until the controller has
  /// been disabled.
  ///
  void Disable();

  ///
  /// Check whether the controller is enabled or not.
  ///
  bool enabled() const { return _enabled; }

  ///
  /// Execute a single iteration of the control procedures. This has to be
  /// implemented based on the controller type.
  ///
  virtual OUTPUT_TYPE DoControl(
    const STATE_TYPE& state, const STATE_TYPE& setpoint) = 0;

  ///
  /// Convenience getters for my children.
  ///
  // Get the delay between executions in seconds.
  double frequency() { return _frequency.count() / double(std::nano::den); }
  void frequency(std::chrono::nanoseconds frequency) { _frequency = frequency; }

  ///
  /// Get the time elapsed since the controller started.
  ///
  double TimeSinceStart();

 private:
  ///
  /// Common variables.
  ///
  // Time-keeping.
  std::chrono::high_resolution_clock::time_point _start_time;
  std::chrono::nanoseconds _frequency;

  // Enabled flag. `true` if we are enabled, `false` otherwise.
  bool _enabled;

  // Worker variable.
  std::unique_ptr<std::thread> _worker;

  // Functions to get various useful values.
  MEASUREMENT_FUNCTION _measurement;
  SETPOINT_FUNCTION _setpoint;
  UPDATE_FUNCTION _update;

  ///
  /// Actually execute the control.
  ///
  void Control();

  ///
  /// Get the current time using C++11's high resolution clock.
  ///
  std::chrono::high_resolution_clock::time_point CurrentTime();
};

#include "controller_impl.h"
