% Constants.
g = 9.81 % m/s^2;
phi_max = deg2rad(20); % maximum tilt angle (configurable on drone) (radians)
theta_max = phi_max;
psi_dot_max = deg2rad(100); % radians/s
z_dot_max = 700/1000; % m/s

% Build A, B, Q and R matrices.
A = zeros(6);
A(1, 4) = 1;
A(2, 5) = 1

B = zeros(6, 4);
B(3, 1) = z_dot_max;
B(4, 2) = phi_max * g;
B(5, 3) = theta_max * g;
B(6, 4) = psi_dot_max

C = eye(6)

Q = diag([
    (2/5)^2
    (2/5)^2
    (1/5)^2
    (1/5)^2
    (1/5)^2
    (1/5)^2
])

R = diag([
  1
  1
  1
  1
])

% Solve LQR.
[K, P, e] = lqr(A, B, Q, R)

% Now, solve the tracking problem! Need to solve the expression:
% -pdot = (A' - PBR^-1B)p - C'Qy(traj))
% Then, u(t) = -K(t)x(t) + K^p(t)p(t)

save('-ascii', 'A.mat', 'A')
save('-ascii', 'B.mat', 'B')
save('-ascii', 'C.mat', 'C')
save('-ascii', 'Q.mat', 'Q')
save('-ascii', 'R.mat', 'R')
save('-ascii', 'K.mat', 'K')
save('-ascii', 'P.mat', 'P')
