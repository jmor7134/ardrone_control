#include "PID.h"
#include "experiment.h"
#include "multi_controller.h"
#include "MPC.h"

#include <functional>

template <int a, int b> using Matrix = dlib::matrix<double, a, b>;
template <int a> using Vector = dlib::matrix<double, a, 1>;

int main(int argc, char** argv) {
  // Create the experiment.
  ros::init(argc, argv, "ardrone_control");
  Experiment e(argc, argv);

  // Build the function path. Make it x = 2x to reduce the length of the path
  // to [0, M_PI] rather than [0, 2*M_PI].
  auto fx = [](double x) {
    x = 2*x;
    return 0.85*sin(2*x) + 0.52*sin(x);
  };

  auto dfdx = [](double x) {
    x = 2 * x;
    return 1.7*cos(2*x) + 0.52*cos(x);
  };

  FunctionPath path(fx, dfdx, 0, M_PI, 1);

  ///
  /// Load parameters.
  ///
  // MPC parameters.
  // Load the A, B, w, Q and R matrices.
  Matrix<kStates, kStates> A;
  A = 1, 0, 0, 1, 0, 0,
      0, 1, 0, 0, 1, 0,
      0, 0, 1, 0, 0, 0,
      0, 0, 0, 1, 0, 0,
      0, 0, 0, 0, 1, 0,
      0, 0, 0, 0, 0, 1;

  const double g = 9.81, phi_max = 0.35, theta_max = 0.35;
  const double psi_dot_max = 1.745, z_dot_max = 0.7;
  Matrix<kStates, kInputs> B;
  B = 0, 0, 0, 0,
      0, 0, 0, 0,
      z_dot_max, 0, 0, 0,
      0, phi_max*g, 0, 0,
      0, 0, theta_max*g, 0,
      0, 0, 0, psi_dot_max;

  Vector<kStates> w;
  w = 0, 0, 0, 0, 0, 0;

  Vector<kStates> Q;
  Q = 3, 3, 1, 0, 0, 1;

  Vector<kInputs> R, lower, upper;
  R = 1, 1, 1, 1;
  lower = -1, -1, -1, -1;
  upper = 1, 1, 1, 1;

  // PID parameters.
  double x_kp, x_ki, x_kd;
  double y_kp, y_ki, y_kd;
  double z_kp, z_ki, z_kd;
  double w_kp, w_ki, w_kd;
  e.nh().param<double>("x_kp", x_kp, 0.50);
  e.nh().param<double>("x_ki", x_ki, 0.00);
  e.nh().param<double>("x_kd", x_kd, 0.35);
  e.nh().param<double>("y_kp", y_kp, 0.50);
  e.nh().param<double>("y_ki", y_ki, 0.00);
  e.nh().param<double>("y_kd", y_kd, 0.35);
  e.nh().param<double>("z_kp", z_kp, 0.60);
  e.nh().param<double>("z_ki", z_ki, 0.00);
  e.nh().param<double>("z_kd", z_kd, 0.10);
  e.nh().param<double>("w_kp", w_kp, 0.00);
  e.nh().param<double>("w_ki", w_ki, 0.00);
  e.nh().param<double>("w_kd", w_kd, 0.05);

  // Path ratio: how heavily weighted the path following should be.
  double path_ratio;
  e.nh().param<double>("path_ratio", path_ratio, 0.9);
  path_ratio = std::max(0.0, std::min(1.0, path_ratio));

  ///
  /// Callback functions for the PID controllers.
  ///   measurement <-- returns the current location (calibrated to zero).
  ///   setpoint <-- returns item [i] of the setpoint vector at time t.
  ///   update <-- updates the references o with the value n.
  ///
  auto measurement = [&e]() {
    const auto& pos = e.pose().position;
    const auto& lin = e.twist().linear;
    const auto& ori = e.pose().orientation;

    const auto& pos_zero = e.zero_pose().position;
    const auto& ori_zero = e.zero_pose().orientation;

    Eigen::VectorXd ret(6);
    ret << pos.x - pos_zero.x, pos.y - pos_zero.y, pos.z - pos_zero.z
         , lin.x, lin.y, ori.w - ori_zero.w;
    return ret;
  };

  auto update = [&e](const Eigen::VectorXd& new_control) {
    e.instructions().linear.x = new_control[0]; // up/down
    e.instructions().linear.y = new_control[1]; // forward/backward
    e.instructions().linear.z = new_control[2]; // left/right
    e.instructions().angular.z = new_control[3]; // rotate left/right
  };

  Eigen::MatrixXd kp = Eigen::MatrixXd::Zero(4, 6);
  kp(0, 0) = x_kp; kp(1, 1) = y_kp; kp(2, 2) = z_kp; kp(3, 5) = w_kp;

  Eigen::MatrixXd ki = Eigen::MatrixXd::Zero(4, 6);
  ki(0, 0) = x_ki; ki(1, 1) = y_ki; ki(2, 2) = z_ki; ki(3, 5) = w_ki;

  Eigen::MatrixXd kd = Eigen::MatrixXd::Zero(4, 6);
  kd(0, 0) = x_kd; kd(1, 1) = y_kd; kd(2, 2) = z_kd; kd(3, 5) = w_kd;

  std::cout << "kp = " << kp << std::endl;

  ///
  /// Create controllers.
  ///
  using namespace std::literals;
  VectorPID pid_p(kp, ki, kd);
  MPC mpc_g(A, B, w, Q, R, lower, upper);

  // Path/Goal controllers.
  MultiController<Eigen::VectorXd, Eigen::VectorXd> multi(
    e.nh(), path, path_ratio, pid_p, mpc_g, 100ms, measurement, update);

  // Run the experiment.
  e({ &multi });
}
