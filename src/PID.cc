#include "PID.h"

PID::PID(
  double kp, double ki, double kd,
  std::chrono::nanoseconds frequency,
  C::MEASUREMENT_FUNCTION measurement,
  C::SETPOINT_FUNCTION setpoint,
  C::UPDATE_FUNCTION update)
  : Controller(frequency, measurement, setpoint, update)
  , _kp(kp), _ki(ki), _kd(kd), _integral(0), _previous_error(0) {

}

PID::~PID() {

}

double PID::DoControl(const double& state, const double& setpoint) {
  double f = frequency();
  double error = setpoint - state;
  double integral = _integral;
  double derivative = (error - _previous_error) / (f);

  _integral += error;
  _previous_error = error;

  return (_kp * error) + (_ki * integral) + (_kd * derivative);
}

VectorPID::VectorPID(Matrix kp, Matrix ki, Matrix kd)
  : Controller(), _kp(kp), _ki(ki), _kd(kd)
  , _integral(Vector::Zero(kp.cols())), _previous_error(Vector::Zero(kp.cols())) {

}

VectorPID::VectorPID(
  Matrix kp, Matrix ki, Matrix kd,
  std::chrono::nanoseconds frequency,
  C::MEASUREMENT_FUNCTION measurement,
  C::SETPOINT_FUNCTION setpoint,
  C::UPDATE_FUNCTION update)
  : Controller(frequency, measurement, setpoint, update)
  , _kp(kp), _ki(ki), _kd(kd)
  , _integral(Vector::Zero(kp.cols())), _previous_error(Vector::Zero(kp.cols())) {

}

VectorPID::~VectorPID() {

}

VectorPID::Vector VectorPID::DoControl(const Vector& state, const Vector& setpoint) {
  double f = frequency();
  Vector error = setpoint - state;
  Vector integral = _integral + error;
  Vector derivative = (error - _previous_error) / f;


  _integral = integral;
  _previous_error = error;

  // std::cout << "state = " << state.transpose() << std::endl;
  // std::cout << "setpoint = " << setpoint.transpose() << std::endl;
  // std::cout << "error = " << error.transpose() << std::endl;
  // std::cout << "previous_error = " << _previous_error.transpose() << std::endl;
  // std::cout << "I = " << error.transpose() << std::endl;
  // std::cout << "D = " << error.transpose() << std::endl;

  Vector u = (_kp * error) + (_ki * integral) + (_kd * derivative);
  // std::cout << "u = " << u.transpose() << std::endl;
  return u;
}
