template <typename S, typename O>
MultiController<S, O>::MultiController(
  ros::NodeHandle& nh,
  FunctionPath path,
  double path_ratio,
  C& path_controller, C& goal_controller,
  std::chrono::nanoseconds frequency,
  MEASUREMENT_FUNCTION measurement,
  UPDATE_FUNCTION update)
  : Controller<S, O>::Controller(frequency, measurement, [](double) { return S(); }, update)
  , _path(path), _path_ratio(path_ratio), _path_controller(&path_controller), _goal_controller(&goal_controller) {
  path_controller.frequency(frequency);
  goal_controller.frequency(frequency);

  _closest_point_eig = Eigen::Vector3d::Zero();
  _closest_point = nh.advertise<geometry_msgs::Point>("/ardrone_control/closest_path_pt", 1000);
  _current_location = nh.advertise<geometry_msgs::Point>("/ardrone_control/estimated_pt", 1000);
}

template <typename S, typename O>
MultiController<S, O>::~MultiController() {

}

template <typename S, typename O>
O MultiController<S, O>::DoControl(const S& state, const S& /* setpoint */) {
  // Work out the currnet path setpoint.
  Eigen::Vector3d loc;
  loc << state(0), state(1), state(2);

  Eigen::Vector3d closest_point = _path.ClosestPoint(loc);

  // If the closest point is to the LEFT of the previous one, then just skip it.
  // Really, who has time to go backward. Geez.
  if (closest_point.x() < _closest_point_eig.x()) {
    closest_point = _closest_point_eig;
  }

  S path_setpoint(S::Zero(state.rows()));
  path_setpoint(0) = closest_point(0);
  path_setpoint(1) = closest_point(1);
  path_setpoint(2) = closest_point(2);

  _closest_point_eig = closest_point;

  Eigen::Vector3d final_point = _path.FinalPoint();
  S final_setpoint(S::Zero(state.rows()));
  final_setpoint(0) = final_point(0);
  final_setpoint(1) = final_point(1);
  final_setpoint(2) = final_point(2);

  // If the closest point and final point are really close together, then just
  // switch to goal-only control.
  double diff = (final_point - closest_point).norm();
  double path_ratio = _path_ratio;
  if (std::abs(diff) < 0.1) {
    path_ratio = 0;
  }

  // Make points and publish them.
  geometry_msgs::Point closest_path_pt, estimated_pt;
  closest_path_pt.x = closest_point.x();
  closest_path_pt.y = closest_point.y();
  closest_path_pt.z = closest_point.z();

  estimated_pt.x = loc.x();
  estimated_pt.y = loc.y();
  estimated_pt.z = loc.z();

  _closest_point.publish(closest_path_pt);
  _current_location.publish(estimated_pt);

  O u_path = _path_controller->DoControl(state, path_setpoint);
  O u_goal = _goal_controller->DoControl(state, final_setpoint);

  // std::cout << "u_goal = " << u_goal.transpose() << std::endl;

  // Should be equivalent to just going for the goal.
  return u_path*path_ratio + u_goal*(1 - path_ratio);
}
