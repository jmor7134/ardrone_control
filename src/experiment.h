#pragma once

#include "controller.h"
#include "path.h"

#include <boost/filesystem.hpp>
#include <ros/ros.h>

// ROS messages.
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Empty.h>

#include <vector>

#include <signal.h>

///
/// An experiment manages a bunch of ros parameters and topics which are common
/// to the different controller types. It should be created before anything else
/// and will block once run.
///
class Experiment {
 public:
  Experiment(int argc, char** argv);
  ~Experiment();

  // Set the path.
  void function_path(FunctionPath& path) { _function_path.reset(&path); }

  // Get references to the ROS internals.
  ros::NodeHandle& nh() { return _nh; }
  ros::Publisher& cmd_vel() { return _cmd_vel; }
  ros::Publisher& land() { return _land; }
  geometry_msgs::Twist& instructions() { return _instructions; }
  const Eigen::Vector3d pose_eig() const;
  const geometry_msgs::Pose& pose() const { return _pose; }
  const geometry_msgs::Twist& twist() const { return _twist; }
  const geometry_msgs::Pose& zero_pose() const { return _zero_pose; }
  const Path& path() const { return *_path; }
  PointPath& point_path() { return *_point_path; }
  const FunctionPath& function_path() { return *_function_path; }

  Eigen::VectorXd current_state() {
    Eigen::VectorXd state(6);
    geometry_msgs::Pose pose = GetAdjustedPose();
    state(0) = pose.position.x;
    state(1) = pose.position.y;
    state(2) = pose.position.z;
    state(3) = _twist.angular.x;
    state(4) = _twist.angular.y;
    state(5) = pose.orientation.z;
    return state;
  }

  // Get the current target point.
  geometry_msgs::Pose GetAdjustedPose();
  geometry_msgs::Pose GetAdjustedTargetPose();

  // Run the experiment.
  void operator()(const std::vector<ControllerInterface*>& controllers);

 private:
  // ROS node handle.
  ros::NodeHandle _nh;

  // ROS topics.
  ros::Publisher _cmd_vel, _land, _estimated_pose, _target;
  ros::Subscriber _state;

  // Message header.
  std_msgs::Header _header;

  // Current odometry returned by SLAM.
  geometry_msgs::Pose _pose;
  geometry_msgs::Twist _twist;

  // Zero pose location (when control started).
  geometry_msgs::Pose _zero_pose;

  // Instructions to send to the drone.
  geometry_msgs::Twist _instructions;

  // Path that this experiment is following.
  std::chrono::high_resolution_clock::time_point _start_time;
  bool _pose_received;
  std::unique_ptr<Path> _path;
  std::unique_ptr<PointPath> _point_path;
  std::unique_ptr<FunctionPath> _function_path;

  // Receive odometry messages.
  void odometry_callback(const nav_msgs::Odometry& msg);
};
