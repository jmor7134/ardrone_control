#pragma once

#include "controller.h"

#include <dlib/control.h>
#include <Eigen/Dense>

// Number of states/controls in our system.
const int kStates = 6;
const int kInputs = 4;
const int kHorizon = 10;
const double kTimeDelta = 0.001; // seconds

//typedef dlib::matrix<double, kStates, 1> STATE_TYPE;
//typedef dlib::matrix<double, kInputs, 1> OUTPUT_TYPE;

typedef Eigen::VectorXd STATE_TYPE;
typedef Eigen::VectorXd OUTPUT_TYPE;

class MPC : public Controller<STATE_TYPE, OUTPUT_TYPE> {
 public:
  typedef Controller<STATE_TYPE, OUTPUT_TYPE> C;

  MPC(
    dlib::matrix<double, kStates, kStates> A,
    dlib::matrix<double, kStates, kInputs> B,
    dlib::matrix<double, kStates, 1> w,
    dlib::matrix<double, kStates, 1> Q,
    dlib::matrix<double, kInputs, 1> R,
    dlib::matrix<double, kInputs, 1> lower,
    dlib::matrix<double, kInputs, 1> upper
  );

  MPC(
    dlib::matrix<double, kStates, kStates> A,
    dlib::matrix<double, kStates, kInputs> B,
    dlib::matrix<double, kStates, 1> w,
    dlib::matrix<double, kStates, 1> Q,
    dlib::matrix<double, kInputs, 1> R,
    dlib::matrix<double, kInputs, 1> lower,
    dlib::matrix<double, kInputs, 1> upper,
    std::chrono::nanoseconds frequency,
    C::MEASUREMENT_FUNCTION measurement,
    C::SETPOINT_FUNCTION setpoint,
    C::UPDATE_FUNCTION update
  );

  ~MPC();

  OUTPUT_TYPE DoControl(const STATE_TYPE& state, const STATE_TYPE& setpoint);

 private:
  // Actual MPC controller.
  dlib::mpc<kStates, kInputs, kHorizon> _controller;
};
