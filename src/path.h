#pragma once

#include "shape.h"

#include <dlib/optimization.h>
#include <Eigen/Dense>
#include <json/json.h>
#include <ros/ros.h>

#include <memory>

///
/// Contains the definitions of a path over time. A path is made up of a bunch
/// of geometric shapes and time intervals. You can evaluate the path at a
/// a specific time and it will return the (x, y, z) position corresponding to
/// that time. When constructed, the path takes as input the maximum time and
/// splits up the path accordingly. It is assumed that the path os cyclic.
///
class Path {
 public:
  Path();
  Path(double runtime);
  ~Path();

  // Add a circle to the path (e.g. the top of a figure 8).
  void SetPreamble(Eigen::Vector3d point, double t);
  void Add(Shape* shape);

  static Path* Load(const std::string& spec_filename);

  // Get the point at a specific time.
  Eigen::Vector3d GetPoint(double t) const;

 private:
  double _runtime;
  std::vector<std::shared_ptr<Shape>> _shapes;
  Eigen::Vector3d _preamble;
  double _preamble_time;
};

///
/// Contains the definition of a path made up of a series of waypoints. This
/// acts more like an iterator, and you see one point at a time. Only once you
/// pass within a certain distance of a point in the path will you progress to
/// the next point.
///
/// As with other paths, this path will loop around once finished.
///
class PointPath {
 public:
  static constexpr double kDefaultDistanceRequired = 0.01; // m

  PointPath(double distance);
  ~PointPath();

  void Add(const Eigen::Vector3d& point);
  static PointPath* Load(const std::string& spec_filename);

  // Get the next point in the path.
  const Eigen::Vector3d& GetPoint(const Eigen::Vector3d& pos);

 private:
  std::vector<Eigen::Vector3d> _points;
  double  _distance_before_advancing;
  int _current_point;
};

///
/// A path which is represented by a function. To keep things simple, we will
/// only support one-dimensional functions. Each path has a start and end point
/// (which is some parameter that must be provided). For best results, the
/// function shouldn't look back on itself.
///
class FunctionPath {
 public:
  // Simple, 2D function type.
  typedef std::function<double(double)> FUNCTION;

  // Create a new function path from the given function object. It should be in
  // the form y = f(x), so the paraeter is `x` and the function returns y.
  // The start and end points are values of `x`. The `z` value is a constant
  // `z` that should be returned with the point.
  FunctionPath(FUNCTION fx, FUNCTION dfdx, double start, double end, double z);
  ~FunctionPath();

  // Return points for various positions on the path.
  Eigen::Vector3d start() const;
  Eigen::Vector3d end() const;
  Eigen::Vector3d operator()(double x) const;

  // Find the closest point from loc to the function.
  Eigen::Vector3d ClosestPoint(const Eigen::Vector3d& loc) const;
  Eigen::Vector3d FinalPoint() const;

 private:
  FUNCTION _fx, _dfdx;
  double _start, _end, _z;
};
