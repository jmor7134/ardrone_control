#include "shape.h"

Shape* Shape::Load(const Json::Value& spec) {
  // Get the type.
  const std::string& type = spec["type"].asString();

  // Switch on the type.
  if (type == "circle") {
    return Circle::Load(spec);
  }

  else if (type == "point") {
    return Point::Load(spec);
  }

  ROS_WARN_STREAM("Unknown shape type " << type);
  return nullptr;
}

Circle::Circle(
  Eigen::Vector3d center, double radius,
  Circle::Direction direction, Circle::Anchor anchor)
  : _center(center), _radius(radius), _direction(direction), _anchor(anchor) {

}

Circle::~Circle() {

}

Eigen::Vector3d Circle::GetPoint(double proportion) const {
  Eigen::Vector3d point;

  double angle = (2.0 * M_PI) * (proportion);
  angle += (_direction * _anchor * (M_PI/180.0));
  if (angle > (2.0*M_PI)) {
    angle -= 2.0*M_PI;
  }

  if (_direction == CLOCK_WISE)
    angle = (2.0*M_PI) - angle;

  point[0] = _center.x() + _radius * std::cos(angle);
  point[1] = _center.y() + _radius * std::sin(angle);
  point[2] = _center.z();

  return point;
}

Circle* Circle::Load(const Json::Value& spec) {
  // Load the center point.
  const Json::Value& center = spec["center"];
  double x = center[0].asDouble();
  double y = center[1].asDouble();
  double z = center[2].asDouble();
  double radius = spec["radius"].asDouble();

  // Load the direction.
  Direction d = CLOCK_WISE;
  const std::string& direction = spec["direction"].asString();
  if (direction == "CLOCK_WISE") {
    d = CLOCK_WISE;
  } else if (direction == "ANTI_CLOCK_WISE") {
    d = ANTI_CLOCK_WISE;
  } else {
    ROS_WARN_STREAM("Unknown circle direction " << direction << ", defaulting to CLOCK_WISE");
  }

  // Load the anchor.
  Anchor a = BOTTOM;
  const std::string& anchor = spec["anchor"].asString();
  if (anchor == "LEFT") {
    a = LEFT;
  } else if (anchor == "RIGHT") {
    a = RIGHT;
  } else if (anchor == "TOP") {
    a = TOP;
  } else if (anchor == "BOTTOM") {
    a = BOTTOM;
  } else {
    ROS_WARN_STREAM("Unknown circle anchor " << anchor << ", defaulting to BOTTOM");
  }

  return new Circle({x, y, z}, radius, d, a);
}

Point::Point(Eigen::Vector3d point) : _point(point) {

}

Point::~Point() {

}

Eigen::Vector3d Point::GetPoint(double /* proportion */) const {
  return _point;
}

Point* Point::Load(const Json::Value& spec) {
  double x = spec["x"].asDouble();
  double y = spec["y"].asDouble();
  double z = spec["z"].asDouble();
  return new Point({x, y, z});
}
