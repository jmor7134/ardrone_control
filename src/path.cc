#include "path.h"

Path::Path() {

}

Path::Path(double runtime) : _runtime(runtime) {

}

Path::~Path() {

}

void Path::SetPreamble(Eigen::Vector3d point, double t) {
  _preamble = point;
  _preamble_time = t;
}

void Path::Add(Shape* shape) {
  _shapes.emplace_back(shape);
}

Path* Path::Load(const std::string& spec_filename) {
  // If the spec is empty, load a default.
  if (spec_filename.size() == 0) {
    Path* path = new Path(20);
    path->Add(new Circle({1, 0, 1}, 1, Circle::CLOCK_WISE, Circle::LEFT));
    path->Add(new Circle({-1, 0, 1}, 1, Circle::ANTI_CLOCK_WISE, Circle::RIGHT));
    return path;
  }

  /// Otherwise, load the path spec.
  // Load the file into a string.
  std::ifstream spec_file(spec_filename);
  std::stringstream buffer;
  buffer << spec_file.rdbuf();

  // Load the spec into a JSON object.
  Json::Value root;
  buffer >> root;

  // If we aren't loading a path, then stop.
  if (root.get("runtime", -1).asDouble() < 0) {
    throw std::runtime_error("invalid path type");
  }

  // Get the runtime.
  double runtime = root.get("runtime", 20.0).asDouble();
  Path* path = new Path(runtime);

  // Otherwise, actually load the path spec.
  for (const Json::Value& spec : root["shapes"]) {
    Shape* new_shape = Shape::Load(spec);
    if (new_shape != nullptr) {
      path->Add(new_shape);
    }
  }

  return path;
}

Eigen::Vector3d Path::GetPoint(double t) const {
  // If we are in the preamble, just return the fixed point.
  if (t < _preamble_time) {
    return _preamble;
  }

  ROS_INFO_ONCE("Finished preamble! Actually starting path.");

  // We are no longer in the preamble! Adjust t accordingly.
  t -= _preamble_time;

  // Scale t to [0, runtime];
  while (t >= _runtime) {
    t -= _runtime;
  }

  // Find what percentage along the runtime we are.
  double percentage_complete = t / _runtime;

  // Figure out which shape we are up to.
  int shape_id = int(percentage_complete * _shapes.size());
  double shape_progress = (percentage_complete * _shapes.size()) - shape_id;
  const Shape& shape = *_shapes[shape_id];

  // Get the point on the shape.
  return shape.GetPoint(shape_progress);
}

///
/// PointPath methods.
///
PointPath::PointPath(double distance)
  : _distance_before_advancing(distance), _current_point(0) {

}

PointPath::~PointPath() {

}

void PointPath::Add(const Eigen::Vector3d& point) {
  _points.push_back(point);
}

PointPath* PointPath::Load(const std::string& spec_filename) {
  if (spec_filename.size() == 0) {
    PointPath* path = new PointPath(kDefaultDistanceRequired);
    path->Add({0, 0, 0});
    return path;
  }

  /// Otherwise, load the spec.
  std::ifstream spec_file(spec_filename);
  std::stringstream buffer;
  buffer << spec_file.rdbuf();

  // Load the JSON object.
  Json::Value root;
  buffer >> root;

  // Get the distance required.
  double distance = root.get("distance_required", kDefaultDistanceRequired).asDouble();
  PointPath* path = new PointPath(distance);

  for (const Json::Value& point : root["points"]) {
    path->Add({point[0].asDouble(), point[1].asDouble(), point[2].asDouble()});
  }

  return path;
}

// Get the next point in the path.
const Eigen::Vector3d& PointPath::GetPoint(const Eigen::Vector3d& pos) {
  // Get the current point.
  if (pos.isApprox(_points[_current_point], _distance_before_advancing)) {
    _current_point = (_current_point + 1) % _points.size();
  }

  return _points[_current_point];
}

FunctionPath::FunctionPath(FUNCTION fx, FUNCTION dfdx, double start, double end, double z)
  : _fx(fx), _dfdx(dfdx), _start(start), _end(end), _z(z) {

}

FunctionPath::~FunctionPath() {

}

Eigen::Vector3d FunctionPath::start() const {
  return (*this)(_start);
}

Eigen::Vector3d FunctionPath::end() const {
  return (*this)(_end);
}

Eigen::Vector3d FunctionPath::operator()(double x) const {
  Eigen::Vector3d p;
  p << x, _fx(x), _z;
  return p;
}

Eigen::Vector3d FunctionPath::ClosestPoint(const Eigen::Vector3d& loc) const {
  using namespace dlib;

  typedef matrix<double, 0, 1> column_vector;

  // Find the closest point on the line. We will do this by building a distance
  // function and then minimizing that function.
  std::function<double(double)> distance_fn = [this, &loc](double x) {
    return sqrt(pow(x - loc.x(), 2) + pow(_fx(x) - loc.y(), 2));
  };

  // Find the minimum of this distance function. It should be a resonable
  // assumption that the minimum distance value occurs near the same `x` value.
  column_vector start(1);
  start(0) = std::min(std::max(loc.x(), _start), _end);

  // double min_x = find_min_single_variable(distance_fn, start, _start, _end);
  find_min_using_approximate_derivatives(
    bfgs_search_strategy(), objective_delta_stop_strategy(1e-7),
    distance_fn, start, -1);

  double min_x = start(0);

  // If the min x is before the start or after the end, cap it.
  min_x = std::min(std::max(min_x, _start), _end);

  Eigen::Vector3d closest_point;
  closest_point << min_x, _fx(min_x), _z;
  return closest_point;
}

Eigen::Vector3d FunctionPath::FinalPoint() const {
  Eigen::Vector3d final_point;
  final_point << _end, _fx(_end), _z;
  return final_point;
}
