#pragma once

#include "controller.h"
#include "path.h"

#include <ros/ros.h>
#include <geometry_msgs/Point.h>

///
/// A multi-controller uses two controllers with specific goals.
///
template <typename STATE_TYPE, typename OUTPUT_TYPE>
class MultiController : public Controller<STATE_TYPE, OUTPUT_TYPE> {
 public:
  typedef Controller<STATE_TYPE, OUTPUT_TYPE> C;
  typedef std::function<STATE_TYPE()> MEASUREMENT_FUNCTION;
  typedef std::function<STATE_TYPE(double)> SETPOINT_FUNCTION;
  typedef std::function<void(const OUTPUT_TYPE&)> UPDATE_FUNCTION;

  MultiController(
    ros::NodeHandle& nh,
    FunctionPath path,
    double path_ratio,
    C& path_controller, C& goal_controller,
    std::chrono::nanoseconds frequency,
    MEASUREMENT_FUNCTION measurement,
    UPDATE_FUNCTION update);
  virtual ~MultiController();

  virtual OUTPUT_TYPE DoControl(
    const STATE_TYPE& state, const STATE_TYPE& setpoint);

 private:
  FunctionPath _path;
  double _path_ratio;
  C* _path_controller;
  C* _goal_controller;

  Eigen::Vector3d _closest_point_eig;


  ros::Publisher _closest_point, _current_location;
};

#include "multi_controller_impl.h"
