#pragma once

#include "controller.h"

#include <Eigen/Dense>

#include <chrono>
#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>

///
/// A Linear Quadratic Regulator (LQR) controller. This takes as input the
/// gain matrix and performs LQR control. The gain matrix should be calculated
/// using a tool like Matlab or Octave. The template parameters are:
///
///   n <-- The number of states.
///   p <-- The number of inputs.
///
class LQT : public Controller<Eigen::VectorXd, Eigen::VectorXd> {
 public:
  typedef Controller<Eigen::VectorXd, Eigen::VectorXd> C;

  // Construct a new LQR.
  LQT(
    Eigen::MatrixXd A, Eigen::MatrixXd B, Eigen::MatrixXd C,
    Eigen::MatrixXd Q, Eigen::MatrixXd R, Eigen::MatrixXd P,
    Eigen::MatrixXd K,
    std::chrono::nanoseconds frequency,
    C::MEASUREMENT_FUNCTION measurement,
    C::SETPOINT_FUNCTION setpoint,
    C::UPDATE_FUNCTION update);

  virtual ~LQT();

 protected:
  Eigen::VectorXd DoControl(
    const Eigen::VectorXd& state, const Eigen::VectorXd& setpoint);

  // Retreive the gain terms K and Ku.
  const Eigen::MatrixXd& Ku(
    const Eigen::VectorXd& state, const Eigen::VectorXd& setpoint);

 private:
  // Gain matrix K.
  Eigen::MatrixXd _K;
  Eigen::MatrixXd _Ku;

  // Keep a copy of the setpoint function.
  C::SETPOINT_FUNCTION _setpoint;

  // Constants needed to perform LQT calculations.
  Eigen::MatrixXd _C1, _C2, _C3;

  // Calculate the additional Ku term.
  void UpdateKu(const Eigen::VectorXd& state, const Eigen::VectorXd& setpoint);
};
