#include "experiment.h"

std::function<void(int)> sigint_handler;

using namespace std::chrono;

Experiment::Experiment(int argc, char** argv)
  : _nh("~"), _start_time(high_resolution_clock::now()), _pose_received(false) {
  /// Load common parameters.
  // Topic names.
  std::string state_node_name, cmd_vel_node_name;
  _nh.param<std::string>("state_node", state_node_name, "/ground_truth/state");
  _nh.param<std::string>("cmd_vel_node", cmd_vel_node_name, "/cmd_vel");

  // Path specification.
  std::string path_spec_filename;
  _nh.param<std::string>("path_spec_filename", path_spec_filename, "");

  // Actually make the nodes.
  _cmd_vel = _nh.advertise<geometry_msgs::Twist>(cmd_vel_node_name, 1000);
  _land = _nh.advertise<std_msgs::Empty>("/ardrone/land", 1);
  _state = _nh.subscribe(
    state_node_name, 1000, &Experiment::odometry_callback, this);

  // Custom nodes (to allow for easy reporting).
  _estimated_pose = _nh.advertise<geometry_msgs::PointStamped>("/ardrone_control/estimated_pose", 1000);
  _target = _nh.advertise<geometry_msgs::PointStamped>("/ardrone_control/target_pose", 1000);

  // Wait until we actually get a pose measurement.
  ros::Rate loop_rate(100);
  ROS_INFO("Waiting for first odometry reading...");
  while (ros::ok() and not _pose_received) {
    ros::spinOnce();
    loop_rate.sleep();
  }

  _zero_pose = _pose;
  ROS_INFO_STREAM("Done! 0 position is " << std::endl << _zero_pose);

  // Load the path specification.
  ROS_INFO_STREAM("Loading path spec from " << path_spec_filename);
  ROS_ASSERT(boost::filesystem::exists(path_spec_filename));

  try {
    _path.reset(Path::Load(path_spec_filename));
    _path->SetPreamble({0, 0, 0}, 2);
  } catch (std::runtime_error) {
    _point_path.reset(PointPath::Load(path_spec_filename));
  }


}

Experiment::~Experiment() {

}

const Eigen::Vector3d Experiment::pose_eig() const {
  Eigen::Vector3d pose = {_pose.position.x - _zero_pose.position.x,
                          _pose.position.y - _zero_pose.position.y,
                          _pose.position.z - _zero_pose.position.z};
  return pose;
}

void Experiment::operator()(const std::vector<ControllerInterface*>& controllers) {
  // Setup the sigint handler.
  sigint_handler = [this, &controllers](int sig) {
    // Disable controllers.
    for (ControllerInterface* c : controllers) {
      c->Disable();
    }

    // Zero all velocities and land the thing.
    cmd_vel().publish(geometry_msgs::Twist());
    land().publish(std_msgs::Empty());

    // Shutdown.
    ros::shutdown();
  };

  signal(SIGINT, [](int sig) { sigint_handler(sig); });

  // Enable all of the controllers.
  ROS_INFO("Starting preamble!");
  for (ControllerInterface* c : controllers) {
    c->Enable();
  }

  // Update the drone and wait.
  ros::Rate loop_rate(1000);
  while (ros::ok()) {
    // Publish new instructions.
    cmd_vel().publish(instructions());

    // Re-publish the pose and target.
    _header.stamp = ros::Time::now();
    _header.seq++;
    _header.frame_id = "map";

    geometry_msgs::PointStamped estimated_pose, target;
    estimated_pose.header = _header;
    target.header = _header;
    estimated_pose.point = GetAdjustedPose().position;
    target.point = GetAdjustedTargetPose().position;

    _estimated_pose.publish(estimated_pose);
    _target.publish(target);

    // Keep on spinning...
    ros::spinOnce();
    loop_rate.sleep();
  }
}

void Experiment::odometry_callback(const nav_msgs::Odometry& msg) {
  _pose_received = true;
  _pose = msg.pose.pose;
  _twist = msg.twist.twist;
}

geometry_msgs::Pose Experiment::GetAdjustedPose() {
  geometry_msgs::Pose p;

  p.position.x = _pose.position.x - _zero_pose.position.x;
  p.position.y = _pose.position.y - _zero_pose.position.y;
  p.position.z = _pose.position.z - _zero_pose.position.z;
  p.orientation.x = _pose.orientation.x - _zero_pose.orientation.x;
  p.orientation.y = _pose.orientation.y - _zero_pose.orientation.y;
  p.orientation.z = _pose.orientation.z - _zero_pose.orientation.z;
  p.orientation.w = _pose.orientation.w - _zero_pose.orientation.w;

  return p;
}

geometry_msgs::Pose Experiment::GetAdjustedTargetPose() {
  Eigen::Vector3d point = Eigen::Vector3d::Zero();
  if (_path.get() != nullptr) {
    auto now = high_resolution_clock::now();
    double t_ns = duration_cast<nanoseconds>(now - _start_time).count();
    auto t = t_ns / std::nano::den;
    point = _path->GetPoint(t);
  } else if (_point_path.get() != nullptr) {
    point = _point_path->GetPoint(pose_eig());
  } else if (_function_path.get() != nullptr) {
    point = _function_path->ClosestPoint(pose_eig());
  }

  geometry_msgs::Pose p;
  p.position.x = point.x();
  p.position.y = point.y();
  p.position.z = point.z();
  p.orientation.x = 0;
  p.orientation.y = 0;
  p.orientation.z = 0;
  p.orientation.w = _zero_pose.orientation.w;

  return p;
}
