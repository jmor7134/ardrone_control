#include "LQR.h"

using Matrix = Eigen::MatrixXd;
using Vector = Eigen::VectorXd;

LQR::LQR(
  Matrix K,
  std::chrono::nanoseconds frequency,
  C::MEASUREMENT_FUNCTION measurement,
  C::SETPOINT_FUNCTION setpoint,
  C::UPDATE_FUNCTION update)
  : Controller(frequency, measurement, setpoint, update)
  , _K(K) {

}

LQR::~LQR() {

}

Matrix LQR::LoadMatrix(const std::string& filename, int rows, int cols) {
  Matrix mat(rows, cols);
  std::ifstream input(filename);

  for (int row = 0 ; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      input >> mat(row, col);
    }
  }

  return mat;
}

Vector LQR::DoControl(const Vector& state, const Vector& setpoint) {
  return -_K * (state - setpoint);
}
