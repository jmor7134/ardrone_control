#pragma once

#include "controller.h"

#include <ros/ros.h>
#include <Eigen/Dense>

#include <chrono>
#include <cmath>
#include <functional>
#include <memory>
#include <ratio>
#include <stdexcept>
#include <thread>

///
/// A PID controller which takes as input some number and some goal and tries to
/// control it. PID controllers run automatically at a specified frequency. A
/// single PID controller should be created for each axis (X, Y, Z).
///
class PID : public Controller<double, double> {
 public:
  typedef Controller<double, double> C;

  // PIDs are constructed using a builder-like pattern.
  PID(
    double kp, double ki, double kd,
    std::chrono::nanoseconds frequency,
    C::MEASUREMENT_FUNCTION measurement,
    C::SETPOINT_FUNCTION setpoint,
    C::UPDATE_FUNCTION update);

  virtual ~PID();

  virtual double DoControl(const double& state, const double& setpoint);

 private:
  // Primary parameters: the proportional, integral and derivative constants.
  double _kp, _ki, _kd;

  // State variables needed to run.
  double _integral, _previous_error;
};

///
/// A class which represents multiple PID controllers working together. Just the
/// same as PID above, but everything is vectorized.
///
class VectorPID : public Controller<Eigen::VectorXd, Eigen::VectorXd> {
 public:
  typedef Eigen::VectorXd Vector;
  typedef Eigen::MatrixXd Matrix;
  typedef Controller<Vector, Vector> C;

  VectorPID(Matrix kp, Matrix ki, Matrix kd);
  VectorPID(
    Matrix kp, Matrix ki, Matrix kd,
    std::chrono::nanoseconds frequency,
    C::MEASUREMENT_FUNCTION measurement,
    C::SETPOINT_FUNCTION setpoint,
    C::UPDATE_FUNCTION update);

  virtual ~VectorPID();

  virtual Vector DoControl(const Vector& state, const Vector& setpoint);

 private:
  // Primary parameters: the proportional, integral and derivative constants.
  Matrix _kp, _ki, _kd;

  // State variables needed to run.
  Vector _integral, _previous_error;
};
