#include "LQT.h"
#include "LQR.h"
#include "experiment.h"

using Matrix = Eigen::MatrixXd;
using Vector = Eigen::VectorXd;

int main(int argc, char** argv) {
  // Create the experiment.
  ros::init(argc, argv, "ardrone_control");
  Experiment e(argc, argv);

  ///
  /// Load parameters.
  ///
  // LQR parameters (p = inputs, n = state variables).
  const int p = 4, n = 6, q = 6;

  // Load K matrix.
  std::string a_filename, b_filename, c_filename;
  std::string q_filename, r_filename, p_filename, k_filename;
  e.nh().param<std::string>("A", a_filename, "A.mat");
  e.nh().param<std::string>("B", b_filename, "B.mat");
  e.nh().param<std::string>("C", c_filename, "C.mat");
  e.nh().param<std::string>("Q", q_filename, "Q.mat");
  e.nh().param<std::string>("R", r_filename, "R.mat");
  e.nh().param<std::string>("P", p_filename, "P.mat");
  e.nh().param<std::string>("K", k_filename, "K.mat");
  ROS_ASSERT(boost::filesystem::exists(a_filename));
  ROS_ASSERT(boost::filesystem::exists(b_filename));
  ROS_ASSERT(boost::filesystem::exists(c_filename));
  ROS_ASSERT(boost::filesystem::exists(q_filename));
  ROS_ASSERT(boost::filesystem::exists(r_filename));
  ROS_ASSERT(boost::filesystem::exists(p_filename));
  ROS_ASSERT(boost::filesystem::exists(k_filename));

  ///
  /// Callback functions for the LQR controller.
  ///   update <-- updates the instructions.
  ///   setpoint <-- gets the setpoint at the current time.
  ///   measurement <-- state measurement into the controller.
  ///
  auto measurement = [&e]() {
    const auto& pos = e.pose().position;
    const auto& lin = e.twist().linear;
    const auto& ori = e.pose().orientation;

    const auto& pos_zero = e.zero_pose().position;
    const auto& ori_zero = e.zero_pose().orientation;

    Vector ret(n);
    ret << pos.x - pos_zero.x, pos.y - pos_zero.y, pos.z - pos_zero.z
         , lin.x, lin.y, ori.w - ori_zero.w;
    return ret;
  };

  auto setpoint = [&e](double t) {
    const Eigen::Vector3d& pt = e.path().GetPoint(t);

    Vector ret(n);
    ret << pt.x(), pt.y(), pt.z(), 0, 0, 0;
    return ret;
  };

  auto update = [&e](const Vector& new_control) {
    e.instructions().linear.z = new_control[0]; // up/down
    e.instructions().linear.x = new_control[1]; // forward/backward
    e.instructions().linear.y = new_control[2]; // left/right
    e.instructions().angular.z = new_control[3]; // rotate left/right
  };

  // Build the lQR controller.
  using namespace std::literals;
  LQT lqt(
    LQR::LoadMatrix(a_filename, n, n), LQR::LoadMatrix(b_filename, n, p),
    LQR::LoadMatrix(c_filename, q, n), LQR::LoadMatrix(q_filename, n, n),
    LQR::LoadMatrix(r_filename, p, p), LQR::LoadMatrix(p_filename, n, n),
    LQR::LoadMatrix(k_filename, p, n), 1ms, measurement, setpoint, update);

  // Run the experiment.
  e({ &lqt });
}
