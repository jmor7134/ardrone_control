template <typename S, typename O>
Controller<S, O>::Controller(
  std::chrono::nanoseconds frequency,
  MEASUREMENT_FUNCTION measurement,
  SETPOINT_FUNCTION setpoint,
  UPDATE_FUNCTION update)
  : _start_time(), _frequency(frequency), _enabled(false), _worker(nullptr)
  , _measurement(measurement), _setpoint(setpoint), _update(update) {

}

template <typename S, typename O>
Controller<S, O>::~Controller() {

}

template <typename S, typename O>
void Controller<S, O>::Enable() {
  _enabled = true;
  _start_time = CurrentTime();
  _worker.reset(new std::thread(&Controller::Control, this));
}

template <typename S, typename O>
void Controller<S, O>::Disable() {
  _enabled = false;

  if (_worker.get() != nullptr) {
    _worker->join();
  }
}

template <typename S, typename O>
void Controller<S, O>::Control() {
  while (enabled()) {
    // Don't over control.
    std::this_thread::sleep_for(_frequency);

    // Actually execute the specific control.
    S x = _measurement(), x_des = _setpoint(TimeSinceStart());
    O u = DoControl(x, x_des);

    // ROS_INFO_STREAM("x = " << x);
    // ROS_INFO_STREAM("x_des = " << x_des);
    // ROS_INFO_STREAM("u = " << u);

    _update(u);
  }
}

using namespace std::chrono;

template <typename S, typename O>
high_resolution_clock::time_point Controller<S, O>::CurrentTime() {
  return high_resolution_clock::now();
}

template <typename S, typename O>
double Controller<S, O>::TimeSinceStart() {
  double t = duration_cast<nanoseconds>(CurrentTime() - _start_time).count();
  return t /  std::nano::den;
}
