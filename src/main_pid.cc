#include "PID.h"
#include "experiment.h"

#include <functional>

int main(int argc, char** argv) {
	// Create the experiment.
  ros::init(argc, argv, "ardrone_control");
  Experiment e(argc, argv);

  ///
  /// Load parameters.
  ///
  // PID parameters.
  double x_kp, x_ki, x_kd;
  double y_kp, y_ki, y_kd;
  double z_kp, z_ki, z_kd;
  double w_kp, w_ki, w_kd;
  e.nh().param<double>("x_kp", x_kp, 0.50);
  e.nh().param<double>("x_ki", x_ki, 0.00);
  e.nh().param<double>("x_kd", x_kd, 0.35);
  e.nh().param<double>("y_kp", y_kp, 0.50);
  e.nh().param<double>("y_ki", y_ki, 0.00);
  e.nh().param<double>("y_kd", y_kd, 0.35);
  e.nh().param<double>("z_kp", z_kp, 0.60);
  e.nh().param<double>("z_ki", z_ki, 0.00);
  e.nh().param<double>("z_kd", z_kd, 0.10);
  e.nh().param<double>("w_kp", w_kp, 0.00);
  e.nh().param<double>("w_ki", w_ki, 0.00);
  e.nh().param<double>("w_kd", w_kd, 0.05);

  ///
  /// Callback functions for the PID controllers.
  ///   measurement <-- returns the current location (calibrated to zero).
  ///   setpoint <-- returns item [i] of the setpoint vector at time t.
  ///   update <-- updates the references o with the value n.
  ///
  auto measurement_x = [&e]() { return e.pose().position.x - e.zero_pose().position.x; };
  auto measurement_y = [&e]() { return e.pose().position.y - e.zero_pose().position.y; };
  auto measurement_z = [&e]() { return e.pose().position.z - e.zero_pose().position.z; };
  auto measurement_w = [&e]() { return e.pose().orientation.w - e.zero_pose().orientation.w; };
  // auto setpoint_x = [&e](double t) { return e.path().GetPoint(t).x(); };
  // auto setpoint_y = [&e](double t) { return e.path().GetPoint(t).y(); };
  // auto setpoint_z = [&e](double t) { return e.path().GetPoint(t).z(); };
  auto setpoint_x = [&e](double /* t */) { return e.point_path().GetPoint(e.pose_eig()).x(); };
  auto setpoint_y = [&e](double /* t */) { return e.point_path().GetPoint(e.pose_eig()).y(); };
  auto setpoint_z = [&e](double /* t */) { return e.point_path().GetPoint(e.pose_eig()).z(); };
  auto setpoint_w = [&e](double /* t */) { return 0; };
  auto update_x = [&e](double n) { e.instructions().linear.x = n; };
  auto update_y = [&e](double n) { e.instructions().linear.y = n; };
  auto update_z = [&e](double n) { e.instructions().linear.z = n; };
  auto update_w = [&e](double n) { e.instructions().angular.z = n; };

  ///
  /// Start controlling!
  ///
  using namespace std::literals;
  PID pid_x(x_kp, x_ki, x_kd, 10ms, measurement_x, setpoint_x, update_x);
  PID pid_y(y_kp, y_ki, y_kd, 10ms, measurement_y, setpoint_y, update_y);
  PID pid_z(z_kp, z_ki, z_kd, 10ms, measurement_z, setpoint_z, update_z);
  PID pid_w(w_kp, w_ki, w_kd, 10ms, measurement_w, setpoint_w, update_w);

  // Run the experiment.
  e({ &pid_x, &pid_y, &pid_z, &pid_w });
}
