#include "MPC.h"
#include "experiment.h"

template <int a, int b> using Matrix = dlib::matrix<double, a, b>;
template <int a> using Vector = dlib::matrix<double, a, 1>;

int main(int argc, char** argv) {
  // Create the experiment.
  ros::init(argc, argv, "ardrone_control");
  Experiment e(argc, argv);

  ///
  /// Load parameters.
  ///
  // Load the A, B, w, Q and R matrices.
  Matrix<kStates, kStates> A;
  A = 1, 0, 0, 1, 0, 0,
      0, 1, 0, 0, 1, 0,
      0, 0, 1, 0, 0, 0,
      0, 0, 0, 1, 0, 0,
      0, 0, 0, 0, 1, 0,
      0, 0, 0, 0, 0, 1;

  const double g = 9.81, phi_max = 0.35, theta_max = 0.35;
  const double psi_dot_max = 1.745, z_dot_max = 0.7;
  Matrix<kStates, kInputs> B;
  B = 0, 0, 0, 0,
      0, 0, 0, 0,
      z_dot_max, 0, 0, 0,
      0, phi_max*g, 0, 0,
      0, 0, theta_max*g, 0,
      0, 0, 0, psi_dot_max;

  Vector<kStates> w;
  w = 0, 0, 0, 0, 0, 0;

  Vector<kStates> Q;
  Q = 3, 3, 1, 0, 0, 1;

  Vector<kInputs> R, lower, upper;
  R = 1, 1, 1, 1;
  lower = -1, -1, -1, -1;
  upper = 1, 1, 1, 1;

  ///
  /// Callback functions for the LQR controller.
  ///   update <-- updates the instructions.
  ///   setpoint <-- gets the setpoint at the current time.
  ///   measurement <-- state measurement into the controller.
  ///
  auto measurement = [&e]() {
    const auto& pos = e.pose().position;
    const auto& lin = e.twist().linear;
    const auto& ori = e.pose().orientation;

    const auto& pos_zero = e.zero_pose().position;
    const auto& ori_zero = e.zero_pose().orientation;

    Eigen::VectorXd ret(kStates);
    ret << pos.x - pos_zero.x, pos.y - pos_zero.y, pos.z - pos_zero.z,
           lin.x, lin.y, ori.w - ori_zero.w;
    return ret;
  };

  // Use this setpoint for paths.
  // auto setpoint = [&e](double t) {
  //   const Eigen::Vector3d& pt = e.path().GetPoint(t);

  //   Vector<kStates> ret;
  //   ret = pt.x(), pt.y(), pt.z(), 0, 0, 0;
  //   return ret;
  // };

  // Use this setpoint for point_paths.
  auto setpoint = [&e](double /* t */) {
    const Eigen::Vector3d& pt = e.point_path().GetPoint(e.pose_eig());

    Eigen::VectorXd ret(kStates);
    ret << pt.x(), pt.y(), pt.z(), 0, 0, 0;
    return ret;
  };

  auto update = [&e](const Eigen::VectorXd& new_control) {
    e.instructions().linear.z = new_control(0); // up/down
    e.instructions().linear.x = -new_control(1); // forward/backward
    e.instructions().linear.y = -new_control(2); // left/right
    e.instructions().angular.z = new_control(3); // rotate left/right
  };

  // Build the lQR controller.
  using namespace std::literals;
  MPC mpc(A, B, w, Q, R, lower, upper, 100ms, measurement, setpoint, update);

  // Run the experiment.
  e({ &mpc });
}
