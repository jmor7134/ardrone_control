#pragma once

#include "controller.h"

#include <Eigen/Dense>

#include <chrono>
#include <functional>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <thread>

///
/// A Linear Quadratic Regulator (LQR) controller. This takes as input the
/// gain matrix and performs LQR control. The gain matrix should be calculated
/// using a tool like Matlab or Octave. The template parameters are:
///
///   n <-- The number of states.
///   p <-- The number of inputs.
///
class LQR : public Controller<Eigen::VectorXd, Eigen::VectorXd> {
 public:
  typedef Controller<Eigen::VectorXd, Eigen::VectorXd> C;

  // Construct a new LQR.
  LQR(
    Eigen::MatrixXd K,
    std::chrono::nanoseconds frequency,
    C::MEASUREMENT_FUNCTION measurement,
    C::SETPOINT_FUNCTION setpoint,
    C::UPDATE_FUNCTION update);

  ~LQR();

  // Load the K matrix from a file.
  static Eigen::MatrixXd LoadMatrix(
    const std::string& filename, int rows, int cols);

  Eigen::VectorXd DoControl(
    const Eigen::VectorXd& state, const Eigen::VectorXd& setpoint);

 private:
  // Gain matrix K.
  Eigen::MatrixXd _K;
};
