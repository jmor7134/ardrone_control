#include "MPC.h"

MPC::MPC(
  dlib::matrix<double, kStates, kStates> A,
  dlib::matrix<double, kStates, kInputs> B,
  dlib::matrix<double, kStates, 1> w,
  dlib::matrix<double, kStates, 1> Q,
  dlib::matrix<double, kInputs, 1> R,
  dlib::matrix<double, kInputs, 1> lower,
  dlib::matrix<double, kInputs, 1> upper)
  : Controller(), _controller(A, B, w, Q, R, lower, upper) {

}

MPC::MPC(
  dlib::matrix<double, kStates, kStates> A,
  dlib::matrix<double, kStates, kInputs> B,
  dlib::matrix<double, kStates, 1> w,
  dlib::matrix<double, kStates, 1> Q,
  dlib::matrix<double, kInputs, 1> R,
  dlib::matrix<double, kInputs, 1> lower,
  dlib::matrix<double, kInputs, 1> upper,
  std::chrono::nanoseconds frequency,
  C::MEASUREMENT_FUNCTION measurement,
  C::SETPOINT_FUNCTION setpoint,
  C::UPDATE_FUNCTION update)
  : Controller(frequency, measurement, setpoint, update)
  , _controller(A, B, w, Q, R, lower, upper) {

}

MPC::~MPC() {

}

OUTPUT_TYPE MPC::DoControl(const STATE_TYPE& state, const STATE_TYPE& setpoint) {
  // Load the setpoint and state into DLIB vectors.
  dlib::matrix<double, kStates, 1> setpoint_dlib, state_dlib;
  setpoint_dlib = setpoint(0), setpoint(1), setpoint(2), setpoint(3), setpoint(4), setpoint(5);
  state_dlib = state(0), state(1), state(2), state(3), state(4), state(5);

  // Return the optimal input.
  _controller.set_target(setpoint_dlib);
  dlib::matrix<double, kInputs, 1> u_dlib = _controller(state_dlib);

  // Convert u back to eigen.
  Eigen::VectorXd u(kInputs);
  u << u_dlib(1), u_dlib(2), u_dlib(0), u_dlib(3);
  return u;
}
